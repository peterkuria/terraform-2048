# 2048 ECS Demo

Hashitalks 2019: 2048 Demo

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| workspace\_iam\_roles |  | map | `<map>` | no |

 <!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
